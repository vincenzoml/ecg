﻿// Learn more about F# at http://fsharp.org

open System

let precedes (a,na) (b,nb) (w : string) =
    let l = w.Length
    let idx c = seq { for i in 0 .. l-1 do if w.[i] = c then yield i } 
    let ia = Array.ofSeq (idx a)
    let ib = Array.ofSeq (idx b)
    ia.[na] < ib.[nb]

type pomset = { 
        nodes : seq<char * int>
        arcs : seq<(char * int) * (char * int)>
    } with 
    member this.GetGraphViz = 
        String.concat 
            "\n"
            (seq {
                yield "digraph POMSET {";
                for (a,na) in this.nodes do yield (a.ToString() + na.ToString() + ";")
                for ((a,na),(b,nb)) in this.arcs do 
                    yield (a.ToString() + na.ToString() + " -> " + b.ToString() + nb.ToString() + ";")
                yield "}"
            })

let dostuff (lang : seq<string>) =
    if Seq.isEmpty lang 
    then { nodes = []; arcs = [] }
    else 
        let dict = System.Collections.Generic.Dictionary()
        for x in Seq.head lang do                         
            dict.[x] <- 
                if dict.ContainsKey x
                then dict.[x] + 1 
                else 1        
        let nodes = 
            seq {
                for x in dict do
                    for i in 0 .. x.Value-1 do
                        yield (x.Key,i) 
            }
        let arcs = seq {
                for n1 in nodes do
                    for n2 in nodes do
                        if Seq.forall (precedes n1 n2) lang then yield (n1,n2)
            }           
        { nodes = nodes; arcs = arcs }
            
[<EntryPoint>]
let main argv =   
    if argv.Length = 0 
    then 
        printfn "usage: ECG filename"
        printfn "filename must be a newline-separated list of words without whitespace"
        1
    else    
        let lang = System.IO.File.ReadAllLines argv.[0]        
        printfn "%s" <| (dostuff lang).GetGraphViz
        0 // return an integer exit code
